import React, { Component } from 'react';

class dynamicYear extends React.Component {
  render() {
    return <span className="currentYear">{(new Date().getFullYear())}</span>;
  }
}
export default dynamicYear;