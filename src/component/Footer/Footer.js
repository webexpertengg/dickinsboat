import React, {Component} from 'react';
import footerLogo from "../../style/images/footer-logo.png";

import Copyright from '../../component/Footer/Copyright';

class IndexFooter extends Component {
    render() {
        return (

            <div>
                <div className="footer-top">
                    <div className="inner-container">
                        <img className="footer-logo img-responsive" src={footerLogo} alt="footerLogo"
                             width="230"
                             height="49"/>
                        <p className="copyright">Copyright Dickins <Copyright/></p>
                        <ul className="social-links">
                            <li><a href="" title="Facebook"><i className="fa fa-facebook-f"></i></a>
                            </li>
                            <li><a href="" title="Twitter"><i className="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="" title="Pinterest"><i
                                className="fa fa-pinterest-p"></i></a></li>
                            <li><a href="" title="Linkedin"><i
                                className="fa fa-linkedin"></i></a></li>
                            <li><a href="" title="Youtube"><i className="fa fa-youtube"></i></a>
                            </li>
                            <li><a href="" title="Instagram"><i className="fa fa-instagram"></i></a>
                            </li>
                            <li><a href="" title="Google"><i
                                className="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div className="inner-container">
                    <ul className="footer-links">
                        <li><a href="/" title="Accueil">Accueil</a></li>
                        <li><a href="/achat-occasion" title="Achat occasion">Achat occasion</a></li>
                        <li><a href="/logistique" title="Logistique">Logistique</a></li>
                        <li><a href="/dickins-and-co" title="Dickins & Co">Dickins & Co</a></li>
                        <li><a href="/temoignage" title="Temoignages">Temoignages</a></li>
                        <li><a href="/infos-légales" title="Infos legales">Infos legales</a></li>
                        <li><a href="/plan-du-site" title="Plan du site">Plan du site</a></li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default IndexFooter;
