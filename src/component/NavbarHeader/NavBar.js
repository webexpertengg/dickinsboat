import React, { Component } from 'react';
import logo from '../../style/images/dickins-logo.png';

class navBar extends Component {
    constructor(props) {
        super(props);
        this.toggleClass = this.toggleClass.bind(this);
        this.state = {
            active: false,
        };
    }
   
    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };
    render() {
        return (
            <header className="header">
                <div className="inner-container">
                    <div className="logo"><a href="/" title="Dickins American Boat Import">
                        <img src={logo} className="App-logo" alt="logo" /></a>
                    </div>
                    <ul className="social-links social-top">
                        <li><a href="" title="Facebook"><i className="fa fa-facebook-f"></i></a>
                        </li>
                        <li><a href="" title="Twitter"><i className="fa fa-twitter"></i></a></li>
                        <li><a href="" title="Pinterest"><i
                            className="fa fa-pinterest-p"></i></a></li>
                        <li><a href="" title="Instagram"><i className="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
                <div className="menu-section navbar"  >
                    <div className="inner-container">
                        <div className="navbar-header">
                            <button type="button" className='navbar-toggle collapsed' onClick={this.toggleClass}
                                data-toggle="collapse"
                                data-target="#myNavbar"><span className="icon-bar"></span><span
                                    className="icon-bar"></span><span className="icon-bar"></span>
                            </button>
                        </div>
                        <nav className={this.state.active ? 'collapse navbar-collapse in' : 'collapse navbar-collapse'} id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="menu-li" id=""><a href="/" title="Accueil">Accueil</a></li>
                                <li className="menu-li" id="achat-occasion"><a href="/achat-occasion" title="Achat occasion">Achat occasion</a></li>
                                <li className="menu-li" id="logistique"><a href="/logistique" title="Logistique">Logistique</a></li>
                                <li className="menu-li" id="dickins-and-co"><a href="/dickins-and-co" title="Dickins & Co">Dickins & Co</a></li>
                                <li className="menu-li" id="temoignage"><a href="/temoignage" title="Temoignages">Temoignages</a></li>
                                <li className="menu-li" id="contact"><a href="/contact" title="Contact">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
        );
    }
}

export default navBar;
