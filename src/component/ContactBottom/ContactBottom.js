import React, {Component} from 'react';
import evenements from '../../style/images/evenements.jpg';



class Contact extends Component {
    render() {
        return (
             <div className="testimonial-section">
                    <div className="inner-container">
                        <div className="row">
                            <div className="col-sm-4 col-xs-12">
                                <div className="col-xs-12">
                                    <div className="testimonial-icon"></div>
                                    <h3 className="heading-black text-center">Témoignages</h3>
                                    <p>Merci Alex pour votre professionalisme après l'achat de ma camaro chez vous.
                                        Merci également pour votre gentillesse et disponibilité.</p>
                                    <p>Je recommande DICKINS sans hésitation pour les prix très compétitifs,
                                        commande,
                                        délai et livraison sans surprise.</p>
                                    <p>Pour ma part je recommanderais votre societe a ma famille et a mes amis
                                        Encore
                                        merci pour tout.</p>
                                    <div className="text-center btn-outer">
                                        <a href="#" title="plus de témoignages"
                                           className="btn blue-btn-caps">plus de témoignages</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4 col-xs-12 border-box">
                                <div className="col-xs-12">
                                    <h3 className="heading-black text-center">NOUS CONTACTER</h3>
                                    <form>
                                        <div className="form-group">
                                            <input type="text" className="form-control noBorder"
                                                   placeholder="NOM & PRENOM"/>
                                        </div>
                                        <div className="form-group">
                                            <input type="email" className="form-control noBorder"
                                                   placeholder="EMAIL "/>
                                        </div>
                                        <div className="form-group">
                                            <input type="tel" className="form-control noBorder"
                                                   placeholder="TELEPHONE"/>
                                        </div>
                                        <div className="form-group">
                                            <textarea className="form-control noBorder"
                                                      placeholder="MESSAGE"></textarea>
                                        </div>
                                        <div className="text-center btn-outer">
                                            <a href="javascript:void(0);" title="ENVOYER"
                                               className="btn blue-btn-caps light-bluebg-btn">ENVOYER</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="col-sm-4 col-xs-12">
                                <div className="col-xs-12">
                                    <h3 className="heading-black text-center">éVèNEMENTS</h3>
                                    <div className="evenements-img">
                                        <img className="img-responsive" src={evenements} alt="product"
                                             width="244"
                                             height="303"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        );
    }
}

export default Contact;
