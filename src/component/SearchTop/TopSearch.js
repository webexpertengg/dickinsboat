import React, {Component} from 'react';


class SearchTop extends Component {
    render() {
        return (
            <div className="search-aside">
                <div className="border-box box-shadow">
                    <div className="row">

                        <div className="col-md-3">
                            <h2 className="heading-bgColor Catalogue">Notre Catalogue 2018</h2>
                        </div>

                        <div className="col-md-9">
                            <div className="border-boxInner border-boxInner1">
                                <div className="row padding-top">
                                    <div className="col-md-6">
                                        <div className="range-slide1">
                                            <div className="range-row">
                                                <input className="range-slider3" type="hidden"
                                                       value="0, 878239"
                                                       style={{display: 'none'}}/>
                                            </div>
                                        </div>

                                        <div className="range-slide2">
                                            <div className="range-row">
                                                <input className="range-slider4" type="hidden"
                                                       value="1960,2018"
                                                       style={{display: 'none'}}/>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-4">
                                        <div className="check-part">
                                            <div className="check-row">
                                                <input type="checkbox"/>
                                                <label>NEUF</label>
                                            </div>
                                            <div className="check-row">
                                                <input type="checkbox"/>
                                                <label>OCCASION</label>
                                            </div>
                                            <div className="check-row">
                                                <input type="checkbox"/>
                                                <label>VOILE</label>
                                            </div>
                                            <div className="check-row">
                                                <input type="checkbox"/>
                                                <label>MOTEUR</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-2 padding-left">
                                        <div className="btn-outer btn-outer1">
                                            <a href="javascript:void(0);" title="Rechercher"
                                               className="btn-lightBlue">Rechercher</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default SearchTop;
