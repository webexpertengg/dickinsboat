import React, {Component} from 'react';
import productFour from '../../style/images/product4.jpg';
import productFive from '../../style/images/product5.jpg';
import productSix from '../../style/images/product6.jpg';

class Business extends Component {
    render() {
        return (
            <div className="full-bg-section">
                    <div className="inner-container">
                        <h3 className="heading-black text-center">LES AFFAIRES DU MOIS à NE PAS MANQUER :</h3>
                        <div className="list-outer list-col4">
                            <div>
                                <div className="product-box">
                                    <div className="list-image">
                                        <img className="img-responsive" src={productFour} alt="product"
                                             width="243" height="135"/>
                                    </div>
                                </div>
                                <div className="detail-box">
                                    <h4 className="heading-xs">ERICSON</h4>
                                    <div className="price-row">450,000 &euro;</div>
                                    <ul className="info-list">
                                        <li>2011</li>
                                        <li>65 m</li>
                                        <li>OVERMARINE</li>
                                    </ul>
                                    <a href="javascript:void(0);" title="EN SAVOIR PLUS"
                                       className="btn blue-btn-caps">en
                                        savoir plus</a>
                                </div>
                            </div>
                            <div>
                                <div className="product-box">
                                    <div className="list-image">
                                        <img className="img-responsive" src={productSix} alt="product"
                                             width="243" height="135"/>
                                    </div>
                                </div>
                                <div className="detail-box">
                                    <h4 className="heading-xs">GREYARROW</h4>
                                    <div className="price-row">500,0000 &euro;</div>
                                    <ul className="info-list">
                                        <li>2005</li>
                                        <li>155 m</li>
                                        <li>OVERMARINE</li>
                                    </ul>
                                    <a href="javascript:void(0);" title="EN SAVOIR PLUS"
                                       className="btn blue-btn-caps">en
                                        savoir plus</a>
                                </div>
                            </div>
                            <div>
                                <div className="product-box">
                                    <div className="list-image">
                                        <img className="img-responsive" src={productFive} alt="product"
                                             width="243" height="135"/>
                                    </div>
                                </div>
                                <div className="detail-box">
                                    <h4 className="heading-xs">WISLEY</h4>
                                    <div className="price-row">625,000 &euro;</div>
                                    <ul className="info-list">
                                        <li>2000</li>
                                        <li>95 m</li>
                                        <li>OVERMARINE</li>
                                    </ul>
                                    <a href="javascript:void(0);" title="EN SAVOIR PLUS"
                                       className="btn blue-btn-caps">en
                                        savoir plus</a>
                                </div>
                            </div>
                            <div>
                                <div className="product-box">
                                    <div className="list-image">
                                        <img className="img-responsive" src={productFour} alt="product"
                                             width="243" height="135"/>
                                    </div>
                                </div>
                                <div className="detail-box">
                                    <h4 className="heading-xs">WISLEY II</h4>
                                    <div className="price-row">350,000 &euro;</div>
                                    <ul className="info-list">
                                        <li>2000</li>
                                        <li>95 m</li>
                                        <li>OVERMARINE</li>
                                    </ul>
                                    <a href="javascript:void(0);" title="EN SAVOIR PLUS"
                                       className="btn blue-btn-caps">en
                                        savoir plus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        );
    }
}

export default Business;
