import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Router} from 'react-router';

import '../../style/css/App.css';
import '../../style/css/bootstrap.min.css';
import '../../style/css/jquery.range.css';
import '../../style/css/stylesheet.css';

import Nav from '../../component/NavbarHeader/NavBar';
import IndexFooter from '../../component/Footer/Footer';

import routes from '../../routes';

export default class App extends Component {
    render() {
      const { history } = this.props;
        return (
          <div>
            <Nav/>
            <Router history={history}>{routes}</Router>
            <IndexFooter/>
          </div>
        );
    }
}
App.propTypes = {
  history: PropTypes.object
};
