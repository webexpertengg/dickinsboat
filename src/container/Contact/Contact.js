import React from 'react';
import temoBanner from '../../style/images/temoignages.jpg';
import contactBg from '../../style/images/contact-background-background.jpg';

import SearchTop from '../../component/SearchTop/TopSearch';
import BusinessOfMonth from '../../component/BusinessDetails/BusinessOfMonth';
import Contact from '../../component/ContactBottom/ContactBottom';
const $ = window.$;


export default class termon extends React.Component {
    constructor(props) {
		super(props);
		this.state = {		
		};
		this.send_email = this.send_email.bind(this);
	}
	
	send_email(event){
		var form = $( "#contact_us" );
		form.validate();
		if(form.valid()){
			var $button = $(event.target);
			var data = new FormData();
			
			$button.button('loading');
			const payload = {
				name: $('#name').val(),
				email: $('#email').val(),
				phone: $('#tel').val(),
				message: $('#message').val(),
			};
			
			data.append("data", JSON.stringify(payload));

			fetch('http://dev.dickinsboat.com/api/sendMail', {
				method: 'POST',
				body: data,
			}).then(function(response) {
				return response.json();
			}).then(function (data) {
				$button.button('reset');
                alert('successfully sent.');
            }).catch(function (err) {
                alert('xxx')
				$button.button('reset');
            })
		} 
		
	}
	
	render() {
        return (
        <div>
            <div className="banner-section">
                <img className="img-responsive" src={temoBanner} alt="Dickins banner image" height="542" width="100%"/>
            </div>
            <div className="inner-pages temoignages">

                <div className="inner-container top-btmPad">
                    <SearchTop />
                </div>
                <div className="inner-container">
                    <div className="contact-heading">
                        <h1 className="heading-lg">Contact</h1>
                        <div className="hr"></div>
                    </div>
                    <div className="conatct-background-tel">
                        <div className="black-box black-box1">
                            <h2 className="heading-md">PAR TELEPHONE</h2>
                            <div className="hr"></div>
                            <p>Vous pouvez appeler l’un de nos representatives de 9H à 19H, du Lundi au Vendredi</p>
                            <a className="black-box-btn" href="">+33 23 30 40 29</a>
                        </div>

                        <div className="black-box black-box2">
                            <h2 className="heading-md">SUR LES RéSEAUX SOCIAUX</h2>
                            <div className="hr"></div>
                            <p>Vous pouvez appeler l’un de nos representatives de 9H à 19H, du Lundi au Vendredi</p>
                            <a className="black-box-btn" href="">contact@dickinsimport.com</a>
                        </div>
                    </div>
                </div>
                <div className="contact-form-background">
                    <img src={contactBg} alt="" />
                    <div className="contact-form">
                            <h2 className="">ECRIVEZ -NOUS </h2>
                        <div className="form-container">
                            <form id="contact_us" action="/action_page.php">
                                <input type="text" id="name" name="name" placeholder="Nom & Prénom" />
                                <input type="text" id="email" name="email" placeholder="EMAIL" type="email" required />
                                <input type="tel" id="tel" name="telephone" placeholder="TELEPHONE" required/>
                                <textarea id="message" name="message" placeholder="MESSAGE" required></textarea>
                                <div className="form-submit-btn">
									<button type="button" className="btn blue-btn-caps light-bluebg-btn" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Sending Email"
									onClick={this.send_email} 
									>ENVOYER</button>
									
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
}