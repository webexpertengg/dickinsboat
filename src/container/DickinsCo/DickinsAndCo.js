import React from 'react';
import dickinsBanner from '../../style/images/dickins-co-banner1.jpg';
import dickins1 from '../../style/images/dickins-co.jpg';
import dickins2 from '../../style/images/dickins-co-banner.jpg';
import dickins3 from '../../style/images/dickins-co1.jpg';

import SearchTop from '../../component/SearchTop/TopSearch';
import BusinessOfMonth from '../../component/BusinessDetails/BusinessOfMonth';
import Contact from '../../component/ContactBottom/ContactBottom';


export default class termon extends React.Component {
    render() {
        return (
        <div>
            <div className="banner-section">
                <img className="img-responsive" src={dickinsBanner} alt="Dickins banner image" height="542" width="100%"/>
            </div>
            <div className="inner-pages temoignages">

                <div className="inner-container top-btmPad">
                    <SearchTop />
                </div>
                <div className="inner-container">
                    <div className="achat-head">
                        <h1 className="heading-lg">DICKINS & Co</h1>
                        <div className="hr">
                        </div>
                        <p>Dickins propose la plus grande flotte de bateaux à vendre dans le monde qui comprend une large gamme de bateauxy compris les bateaux à moteur et bateaux à voile classiques,</p>
                    </div>
                </div>
                <div className="achat-part">
                    <div className="row">
                        <div className="col-md-7">
                            <img alt="" src={dickins1} />
                        </div>
                        <div className="col-md-5">
                            <div className="achat-part-content">
                               <h3 className="heading-black">ACHETER UN BATEAU</h3>
                               <p className="padding-0">Dickins  propose la plus grande flotte de bateaux à vendre dans le monde qui comprend une large gamme de yachts y compris les<br></br>
                               bateaux à moteur et bateaux à voile classiques, . Nous avons également accès à des milliers d'autres, y compris ceux qui ne sont pas cotés en bourse. Nous travaillons main dans la main avec nos clients pour les guider et les conseiller sur tous les aspects du processus d'achat. Vous pouvez rechercher des yachts en utilisant la fonction de recherche spécialisée ci-dessus.</p>

                               <div className="importer-content">
                                   <h3 className="heading-black">NOTRE  EXPERTISE A VOTRE SERVICE</h3>
                                   <p className="padding-0">La vente ou l'achat d'un bateau exige l'expertise professionnelle et la connaissance étendue du marché. En mettant l'accent sur la vente et l'achat de yachts de plus de 30 mètres, notre grande équipe de courtiers hautement expérimentés visite des centaines de bateaux chaque année et les chantiers navals les plus importants au monde. Nous sommes en contact régulier avec des centaines de propriétaires de bateaux et d'acheteurs potentiels, leur fournissant une compréhension approfondie du marché, tandis que notre base de données avancée nous permet de rapprocher les acheteurs et les vendeurs avec la plus grande efficacité. </p>
                               </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="banner-bottom banner-bottom-copy">
                    <img src={dickins2} alt="Rochelle" />
                </div>
                <div className="banner-content">
                    <div className="inner-container">
                        <div className="banner-content-copy">
                            <h2 className="heading-md heading-md-copy">Importer un bateau des Etats-Unis</h2>
                            <div className="hr"></div>
                            <p>L'achat d'un bateau est l'une des réalisations majeures de la vie. Cependant, le défi de trouver le yacht parfait au bon prix n'est pas un voyage à entreprendre à la légère ou sans une main expérimentée pour vous guider.parfaite pour vous garantir une expérience inoubliable.</p>
                        </div>
                    </div>

                    <div className="pourqout">
                        <div className="row">
                            <div className="col-md-5">
                                <div className="achat-part-content achat-part-content-copy">
                                    <div className="importer-content1">
                                        <h3 className="heading-black">POURQUOI  IMPORTER ?</h3>
                                        <div className="hr"></div>
                                        <p className="padding-0">Une location de yacht de luxe offre tout ce que vous pouvez imaginer et plus, de la relaxation absolue à l'aventure à couper le souffle et aux moments précieux de la famille, c'est l'évasion ultime. La location d'un yacht vous permet d'explorer une variété de destinations tout en vous offrant tout le confort et les commodités d'une villa de luxe. Les possibilités sont infinies, mais votre courtier spécialisé dans l'affrètement découvrira vos besoins et vous guidera tout au long du processus de sélection, vous aidant ainsi à faire le choix final et à négocier le meilleur taux pour vous.</p>
                                    </div>
                                    <div className="importer-content">
                                        <h3 className="heading-black">LES CHOSES A SAVOIR</h3>
                                        <div className="hr"></div>
                                        <p className="padding-0">Le transport maritime est le mode de transport le plus important pour le transport de marchandises (marine marchande).
                                        La flotte marchande mondiale est constituée de différents types de vaisseaux. Au début de 2007, la flotte mondiale a, en comptabilisant les navires de plus de 1000 tpl, pour la première fois, dépassé 1 milliard de tonnes de port en lourd.</p>
                                        <div className="plus-btn">
                                            <button type="button" class="btn blue-btn">EN SAVOIR PLUS</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-7">
                                <img alt="" src={dickins3} />
                            </div>
                        </div>
                    </div>
                </div>
                <BusinessOfMonth/>
                <Contact/>
            </div>
        </div>);
    }
}