//https://reactjs.org/docs/faq-ajax.html
//https://daveceddia.com/ajax-requests-in-react/
//https://www.robinwieruch.de/react-fetching-data/
// pagination
//https://stackoverflow.com/questions/40232847/how-to-implement-pagination-in-reactjs
//https://codepen.io/PiotrBerebecki/pen/pEYPbY

import React, {Component} from 'react';
import homeBanner from '../../style/images/dickin-banner.jpg';
import americanBoatLogo from '../../style/images/american-boat-logo.png';
import transparentLogo from '../../style/images/transparent-logo.png';
import importLogo from '../../style/images/import-logo.png';


import Contact from '../../component/ContactBottom/ContactBottom';
import BusinessOfMonth from '../../component/BusinessDetails/BusinessOfMonth';

class Index extends Component {
	
	constructor(props) {
		super(props);
		this.api_end_point = 'http://dev.dickinsboat.com/api/cars2';
		this.state = {		
		  data: null,
		  currentPage: 1,
		  totalPages:1
		};
		this.get_next_page = this.get_next_page.bind(this);
	}
	
	componentDidMount() {
		fetch(this.api_end_point)
		  .then(response => response.json())
		  .then(res => {
				console.log(res);
				this.setState({ 
					isLoaded: true, 
					totalPages: res.total_pages, 
					cars: res.cars });
			});
	}
	
	get_next_page(event) {
		this.setState({
		  isLoaded: false,	
		  currentPage: Number(event.target.id)
		});
		
		fetch(this.api_end_point + '?page=' + event.target.id)
		  .then(response => response.json())
		  .then(res => {
				this.setState({ 
					isLoaded: true, 
					totalPages: res.total_pages, 
					cars: res.cars });
			
			});
	  }
	
    render() {
		const { currentPage, totalPages, error, isLoaded, cars } = this.state;
        if (error) {
			return <div>Error: {error.message}</div>;
		} else if (!isLoaded) {
			return <div>Loading...</div>;
		} else{
			function page_numbers(currentPage, totalPages){	
				var maxPageMargin = 3;
				var left = [];
				var right = [];
				if(currentPage > 1){
					for(var i=(currentPage-1); i > 0 && currentPage-i <= 3; i--){
						left.push(i);
					}
				}
				
				if (currentPage < totalPages){
					for (var i=(currentPage + 1); currentPage <=totalPages && i-currentPage<=3; i++ ){
						right.push(i);
					}
				}
				return left.reverse().concat([currentPage]).concat(right);
			}
			const pageNumbers = page_numbers(currentPage, totalPages)

			const renderPageNumbers = pageNumbers.map(number => {
			  var active_class = (number == currentPage) ? 'page-item active' : 'page-item';
			  //alert(number == currentPage);
			  return (
				<li className={active_class}>
					<a className="page-link" 
						onClick={this.get_next_page} 
						key={number}
						id={number}
						href="javascript:void(0);">{number}</a>
				</li>
			  );
			});
		
			return (
				<div>
					<div className="banner-section">
						<img className="banner-img" src={homeBanner} alt="image"/>
					</div>
					<div className="inner-container top-btmPad">
						<div className="row-custom">
							<div className="col-left">
								<div className="search-aside">

									<div className="border-box">
										<h2 className="heading-bgColor">Notre Catalogue 2018</h2>
										<div className="border-boxInner">
											<div className="input-area">
												<input className="form-control" type="text"
													   placeholder="Entrez votre recherche"/>
											</div>
											<h4 className="bold-label">Prix</h4>
											<div className="range-row">
												<input className="range-slider" type="hidden" value="0,2000000"
													   style={{display: 'none'}}/>
											</div>
											<h4 className="bold-label">Type</h4>
											<div className="row">
												<div className="col-md-6 col-xs-12 check-row">
													<input type="checkbox" id="voile"/>
													<label>Voile (3312)</label>
												</div>
												<div className="col-md-6 col-xs-12 check-row">
													<input type="checkbox" id="moteur"/>
													<label>Moteur (6453)</label>
												</div>
											</div>
											<h4 className="bold-label">Etat</h4>
											<div className="row">
												<div className="col-md-6 col-xs-12 check-row">
													<input type="checkbox"/>
													<label>NEUF (3312)</label>
												</div>
												<div className="col-md-6 col-xs-12 check-row">
													<input type="checkbox"/>
													<label>OCCASION (6453)</label>
												</div>
											</div>
											<h4 className="bold-label">Années</h4>
											<div className="range-row">
												<input className="range-slider2" type="hidden" value="1960,2018"
													   style={{display: 'none'}}/>
											</div>
											<h4 className="bold-label">Marques</h4>
											<div className="check-row">
												<input type="checkbox"/>
												<label>SEA RAY (383)</label>
											</div>
											<div className="check-row">
												<input type="checkbox"/>
												<label>VIKING (224)</label>
											</div>
											<div className="check-row">
												<input type="checkbox"/>
												<label>VIKING (224)</label>
											</div>
											<div className="check-row">
												<input type="checkbox"/>
												<label>VIKING (224)</label>
											</div>
											<div className="check-row">
												<input type="checkbox"/>
												<label>VIKING (224)</label>
											</div>
											<div className="check-row">
												<input type="checkbox"/>
												<label>VIKING (224)</label>
											</div>
											<a href="javascript:void(0);" title="Plus de margues" className="text-link">Plus
												de
												margues</a>
											<div className="btn-outer">
												<a href="javascript:void(0);" title="Rechercher"
												   className="btn-lightBlue">Rechercher</a>
											</div>
										</div>
									</div>

									<div className="space-box text-center">
										<img className="img-responsive" src={transparentLogo} alt="Logo"
											 width="145"
											 height="146"/>
									</div>

									<div className="border-box">
										<h2 className="heading-bgColor">RECEVER LA nEWSLETTER</h2>
										<div className="border-boxInner">
											<div className="image-bgBox">
												<p>DES DIZAINES DE NOUVELLES OFFRES CHAQUE JOUR</p>
												<i className="fa fa-star"></i>
												<p>UN CONTROLE DE QUALITE SUR CHAQUE ARTICLE</p>
												<i className="fa fa-star"></i>
												<p>UN SUIVI DETAILLé JUSQU’A Réception de votre bateau</p>
												<div className="input-bgBox">
													<input type="text" className="trans-control" placeholder="Email"/>
												</div>
											</div>
											<div className="btn-outer">
												<a href="javascript:void(0);" title="Rechercher"
												   className="btn-lightBlue btn-blue">Rechercher</a>
											</div>
										</div>
									</div>

								</div>
							</div>
							<div className="col-right">
								<h1 className="heading-lg">BIENVENUE SUR DICKINS <span>SPéCIALISTE EN IMPORTATION DE BATEAUX DES USA</span>
								</h1>
								<div className="round-ibox">
									<div className="colm-left">
										<span className="count-col">4455</span>
									</div>
									<div className="round-middle">
										<label>classer par</label>
										<div className="select-col">
											<select className="form-control">
												<option>Choisir un classement</option>
												<option>Classement two</option>
												<option>Classement three</option>
											</select>
										</div>
									</div>
									<div className="round-right">
										 <img className="img-responsive" src={importLogo} alt="" height="49" width="239"/>
									</div>
								</div>

								<div className="list-outer">
									{cars.map(car => (								
										<div>
											<div className="product-box">
												<div className="list-image">
													<img className="img-responsive" src={car.image} alt="product"
														 width="243" height="135"/>
												</div>
												<div className="product-detail-box">
													<h3 className="product-detail-title">{car.title}</h3>
													<div className="product-detail-subtitle">{car.brand}, {car.model}</div>
													<div className="product-amount">{car.price} &euro;</div>
													<button type="button" className="btn blue-btn">Détails</button>
												</div>
											</div>
										</div>						  
									))}	
								</div>

								<div className="product-pagination text-center">
									<ul className="pagination">
										<li className="page-item">
											<a className="page-link" href="#" title="Previous" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												<span className="sr-only">Previous</span>
											</a>
										</li>
										{renderPageNumbers}
										<li className="page-item">
											<a className="page-link" href="#" title="Next" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												<span className="sr-only">Next</span>
											</a>
										</li>
									</ul>
								</div>

							</div>
						</div>
					</div>
					<BusinessOfMonth/>
					<div className="bottom-bg-banner">
						<div className="inner-container">
							<div className="american-import text-center">
								<div className="white-logo">
									<img className="img-responsive" src={americanBoatLogo} alt="american-boat Import"
										 width="366"
										 height="99"/>
								</div>
								<div className="banner-caps-title">
									DIRECTEMENT LIVRé dAns LA marina de votre choix
								</div>
								<a href="javascript:void(0);" title="plus de témoignages" className="btn btn-black">NOS
									GARANTIES</a>
							</div>
						</div>
					</div>
					<Contact/>

				</div>

			);
		}
	}
}

export default Index;
