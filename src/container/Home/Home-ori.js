import React, {Component} from 'react';
import homeBanner from '../../style/images/dickin-banner.jpg';
import productOne from '../../style/images/product1.jpg';
import productTow from '../../style/images/product2.jpg';
import productThree from '../../style/images/product3.jpg';
import productFour from '../../style/images/product4.jpg';
import productFive from '../../style/images/product5.jpg';
import productSix from '../../style/images/product6.jpg';
import americanBoatLogo from '../../style/images/american-boat-logo.png';
import transparentLogo from '../../style/images/transparent-logo.png';
import importLogo from '../../style/images/import-logo.png';


import Contact from '../../component/ContactBottom/ContactBottom';
import BusinessOfMonth from '../../component/BusinessDetails/BusinessOfMonth';

class Index extends Component {
    render() {
        return (
            <div>
                <div className="banner-section">
                    <img className="banner-img" src={homeBanner} alt="image"/>
                </div>
                <div className="inner-container top-btmPad">
                    <div className="row-custom">
                        <div className="col-left">
                            <div className="search-aside">

                                <div className="border-box">
                                    <h2 className="heading-bgColor">Notre Catalogue 2018</h2>
                                    <div className="border-boxInner">
                                        <div className="input-area">
                                            <input className="form-control" type="text"
                                                   placeholder="Entrez votre recherche"/>
                                        </div>
                                        <h4 className="bold-label">Prix</h4>
                                        <div className="range-row">
                                            <input className="range-slider" type="hidden" value="0,2000000"
                                                   style={{display: 'none'}}/>
                                        </div>
                                        <h4 className="bold-label">Type</h4>
                                        <div className="row">
                                            <div className="col-md-6 col-xs-12 check-row">
                                                <input type="checkbox" id="voile"/>
                                                <label>Voile (3312)</label>
                                            </div>
                                            <div className="col-md-6 col-xs-12 check-row">
                                                <input type="checkbox" id="moteur"/>
                                                <label>Moteur (6453)</label>
                                            </div>
                                        </div>
                                        <h4 className="bold-label">Etat</h4>
                                        <div className="row">
                                            <div className="col-md-6 col-xs-12 check-row">
                                                <input type="checkbox"/>
                                                <label>NEUF (3312)</label>
                                            </div>
                                            <div className="col-md-6 col-xs-12 check-row">
                                                <input type="checkbox"/>
                                                <label>OCCASION (6453)</label>
                                            </div>
                                        </div>
                                        <h4 className="bold-label">Années</h4>
                                        <div className="range-row">
                                            <input className="range-slider2" type="hidden" value="1960,2018"
                                                   style={{display: 'none'}}/>
                                        </div>
                                        <h4 className="bold-label">Marques</h4>
                                        <div className="check-row">
                                            <input type="checkbox"/>
                                            <label>SEA RAY (383)</label>
                                        </div>
                                        <div className="check-row">
                                            <input type="checkbox"/>
                                            <label>VIKING (224)</label>
                                        </div>
                                        <div className="check-row">
                                            <input type="checkbox"/>
                                            <label>VIKING (224)</label>
                                        </div>
                                        <div className="check-row">
                                            <input type="checkbox"/>
                                            <label>VIKING (224)</label>
                                        </div>
                                        <div className="check-row">
                                            <input type="checkbox"/>
                                            <label>VIKING (224)</label>
                                        </div>
                                        <div className="check-row">
                                            <input type="checkbox"/>
                                            <label>VIKING (224)</label>
                                        </div>
                                        <a href="javascript:void(0);" title="Plus de margues" className="text-link">Plus
                                            de
                                            margues</a>
                                        <div className="btn-outer">
                                            <a href="javascript:void(0);" title="Rechercher"
                                               className="btn-lightBlue">Rechercher</a>
                                        </div>
                                    </div>
                                </div>

                                <div className="space-box text-center">
                                    <img className="img-responsive" src={transparentLogo} alt="Logo"
                                         width="145"
                                         height="146"/>
                                </div>

                                <div className="border-box">
                                    <h2 className="heading-bgColor">RECEVER LA nEWSLETTER</h2>
                                    <div className="border-boxInner">
                                        <div className="image-bgBox">
                                            <p>DES DIZAINES DE NOUVELLES OFFRES CHAQUE JOUR</p>
                                            <i className="fa fa-star"></i>
                                            <p>UN CONTROLE DE QUALITE SUR CHAQUE ARTICLE</p>
                                            <i className="fa fa-star"></i>
                                            <p>UN SUIVI DETAILLé JUSQU’A Réception de votre bateau</p>
                                            <div className="input-bgBox">
                                                <input type="text" className="trans-control" placeholder="Email"/>
                                            </div>
                                        </div>
                                        <div className="btn-outer">
                                            <a href="javascript:void(0);" title="Rechercher"
                                               className="btn-lightBlue btn-blue">Rechercher</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-right">
                            <h1 className="heading-lg">BIENVENUE SUR DICKINS <span>SPéCIALISTE EN IMPORTATION DE BATEAUX DES USA</span>
                            </h1>
                            <div className="round-ibox">
                                <div className="colm-left">
                                    <span className="count-col">4455</span>
                                </div>
                                <div className="round-middle">
                                    <label>classer par</label>
                                    <div className="select-col">
                                        <select className="form-control">
                                            <option>Choisir un classement</option>
                                            <option>Classement two</option>
                                            <option>Classement three</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="round-right">
                                     <img className="img-responsive" src={importLogo} alt="" height="49" width="239"/>
                                </div>
                            </div>

                            <div className="list-outer">
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productOne} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2010 CODSCASA MEGA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, SEA RAY</div>
                                            <div className="product-amount">350,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productTow} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2012 WANTA SSA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, VICKING</div>
                                            <div className="product-amount">450,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productThree} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2014 ODDISSEY</h3>
                                            <div className="product-detail-subtitle">Hord Bord, HATTERAS</div>
                                            <div className="product-amount">390,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productFour} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2010 CODSCASA MEGA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, SEA RAY</div>
                                            <div className="product-amount">350,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productFive} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2012 WANTA SSA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, VICKING</div>
                                            <div className="product-amount">450,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productSix} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2014 ODDISSEY</h3>
                                            <div className="product-detail-subtitle">Hord Bord, HATTERAS</div>
                                            <div className="product-amount">390,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productOne} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2010 CODSCASA MEGA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, SEA RAY</div>
                                            <div className="product-amount">350,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productTow} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2012 WANTA SSA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, VICKING</div>
                                            <div className="product-amount">450,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productThree} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2014 ODDISSEY</h3>
                                            <div className="product-detail-subtitle">Hord Bord, HATTERAS</div>
                                            <div className="product-amount">390,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productFour} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2010 CODSCASA MEGA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, SEA RAY</div>
                                            <div className="product-amount">350,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productFive} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2012 WANTA SSA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, VICKING</div>
                                            <div className="product-amount">450,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productSix} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2014 ODDISSEY</h3>
                                            <div className="product-detail-subtitle">Hord Bord, HATTERAS</div>
                                            <div className="product-amount">390,000 &euro;</div>
                                            <button type="button" className="btn blue-btn">Détails</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="product-pagination text-center">
                                <ul className="pagination">
                                    <li className="page-item">
                                        <a className="page-link" href="#" title="Previous" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span className="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <li className="page-item active"><a className="page-link" href="#">1</a></li>
                                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                                    <li className="page-item"><a className="page-link" href="#">4</a></li>
                                    <li className="page-item"><a className="page-link" href="#">5</a></li>
                                    <li className="page-item"><a className="page-link" href="#">6</a></li>
                                    <li className="page-item"><a className="page-link" href="#">7</a></li>
                                    <li className="page-item">
                                        <a className="page-link" href="#" title="Next" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span className="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <BusinessOfMonth/>
                <div className="bottom-bg-banner">
                    <div className="inner-container">
                        <div className="american-import text-center">
                            <div className="white-logo">
                                <img className="img-responsive" src={americanBoatLogo} alt="american-boat Import"
                                     width="366"
                                     height="99"/>
                            </div>
                            <div className="banner-caps-title">
                                DIRECTEMENT LIVRé dAns LA marina de votre choix
                            </div>
                            <a href="javascript:void(0);" title="plus de témoignages" className="btn btn-black">NOS
                                GARANTIES</a>
                        </div>
                    </div>
                </div>
                <Contact/>

            </div>

        );
    }
}

export default Index;
