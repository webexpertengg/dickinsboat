import React, {Component} from 'react';
import InputRange from 'react-input-range';
import homeBanner from '../../style/images/dickin-banner.jpg';
import americanBoatLogo from '../../style/images/american-boat-logo.png';
import transparentLogo from '../../style/images/transparent-logo.png';
import importLogo from '../../style/images/import-logo.png';
import 'react-input-range/lib/css/index.css';
import Contact from '../../component/ContactBottom/ContactBottom';
import BusinessOfMonth from '../../component/BusinessDetails/BusinessOfMonth';
const MAX_ITEMS = 2;
class Index extends Component {
	
    constructor(props) {
        super(props);
		this.api_end_point = 'http://dev.dickinsboat.com/api/cars2';
		this.brands_end_point = 'http://dev.dickinsboat.com/api/brands';
		this.listingRef = React.createRef();
		this.get_next_page = this.get_next_page.bind(this);
        this.changeAmountRange = this.changeAmountRange.bind(this);
        this.changeYearRange = this.changeYearRange.bind(this);
        this.getFilters = this.getFilters.bind(this);
        this.getBrandFilters = this.getBrandFilters.bind(this);
        this.getSearchtxt = this.getSearchtxt.bind(this);
        this.state = {
          amountValue: { min: 0, max: 2000000 },
          yearValue:{ min: 1960, max: 2018 },
          searchtxt:'',
		  searchtxt:'',
          minAmount:1000,
          maxAmount:30000,
		  data: null,
		  currentPage: 1,
		  totalPages:1,
		  totalCars:0,
		  brands: [],
		  brandFilters: [],
		  showMoreBrands: false,
        };
      }
      componentDidMount() {
        fetch(this.api_end_point)
          .then(response => response.json())
          .then(res => {
				this.setState({ 
					isLoaded: true, 
					totalPages: res.total_pages, 
					totalCars: res.total_cars, 
					cars: res.cars });
			});
			
		fetch(this.brands_end_point)
          .then(response => response.json())
          .then(res => {
				this.setState({brands: res.brands });
			});    
    }
	
	showMoreBrandToggle = () => {
		this.setState({ showMoreBrands: !this.state.showMoreBrands });
    }
	
	getRenderedBrands() {
		if (this.state.showMoreBrands) {
			return this.state.brands;
		}
		return this.state.brands.slice(0, MAX_ITEMS);
	}
	scrollToMyRef = () => window.scrollTo(0, this.listingRef.current.offsetTop) ;
	
    changeAmountRange(val) {
       this.setState({ amountValue: val });
    };
    changeYearRange(val) {
        this.setState({ yearValue: val });
     };
     getFilters(){
		this.setState({
		  isLoaded: false,	
		  currentPage: 1,
		});
		var api_end_point = this.api_end_point;
		var filters = []
		if(this.state.searchtxt !=''){
			filters.push('s=' + this.state.searchtxt);
		}
		if(this.state.amountValue.min && this.state.amountValue.max){
			filters.push('min_price=' + this.state.amountValue.min);
			filters.push('max_price=' + this.state.amountValue.max);
		}
		
		if(this.state.yearValue.min && this.state.yearValue.max){
			filters.push('min_year=' + this.state.yearValue.min);
			filters.push('max_year=' + this.state.yearValue.max);
		}
		
		if(this.state.brandFilters.length){
			//this.state.brandFilters.map(brand=> console.log(brand));
			filters.push('brands=' + this.state.brandFilters.join())
		}
		
		api_end_point += '?' + filters.join('&')
		
		fetch(api_end_point)
		  .then(response => response.json())
		  .then(res => {
				this.setState({ 
					isLoaded: true, 
					totalCars: res.total_cars,  
					totalPages: res.total_pages, 
					cars: res.cars });
			
			});
     };
     
	 getBrandFilters(e){
		var filters = this.state.brandFilters;
		if(e.target.checked ){
			if(!filters.includes(e.target.value))
				filters.push(e.target.value);
		} else {
			filters = filters.filter(function(ele){
				return ele != e.target.value;
			});
		}
		this.setState({ brandFilters: filters });
	 }
	 
     getSearchtxt(e){
        this.setState({ searchtxt: e.target.value });
     };
	 
	 get_next_page(event) {
		this.setState({
		  isLoaded: false,	
		  currentPage: Number(event.target.id)
		});
		
		var api_end_point = this.api_end_point;
			
		
		var filters = ['page=' + event.target.id]
		if(this.state.searchtxt !=''){
			filters.push('s=' + this.state.searchtxt);
		}
		if(this.state.amountValue.min && this.state.amountValue.max){
			filters.push('min_price=' + this.state.amountValue.min);
			filters.push('max_price=' + this.state.amountValue.max);
		}
		
		if(this.state.yearValue.min && this.state.yearValue.max){
			filters.push('min_year=' + this.state.yearValue.min);
			filters.push('max_year=' + this.state.yearValue.max);
		}
		
		api_end_point += '?'+ filters.join('&');
		fetch(api_end_point)
		  .then(response => response.json())
		  .then(res => {
				this.setState({ 
					isLoaded: true, 
					totalCars: res.total_cars, 
					totalPages: res.total_pages, 
					cars: res.cars });
				 return this.scrollToMyRef();
			});
	  }

    render() {
    	const { currentPage, totalPages, totalCars, error, isLoaded, cars } = this.state;
        
		if (error) {
			return <div>Error: {error.message}</div>;
		} else{
			function page_numbers(currentPage, totalPages){	
				var maxPageMargin = 3;
				var left = [];
				var right = [];
				if(currentPage > 1){
					for(var i=(currentPage-1); i > 0 && currentPage-i <= 3; i--){
						left.push(i);
					}
				}
				
				if (currentPage < totalPages){
					for (var i=(currentPage + 1); currentPage <=totalPages && i-currentPage<=3; i++ ){
						right.push(i);
					}
				}
				return left.reverse().concat([currentPage]).concat(right);
			}
			const pageNumbers = page_numbers(currentPage, totalPages)

			const renderPageNumbers = pageNumbers.map(number => {
			  var active_class = (number == currentPage) ? 'page-item active' : 'page-item';
			  //alert(number == currentPage);
			  return (
				<li className={active_class}>
					<a className="page-link" 
						onClick={this.get_next_page} 
						key={number}
						id={number}
						href="javascript:void(0);">{number}</a>
				</li>
			  );
			});
		
			return (
				<div>
					<div className="banner-section">
						<img className="banner-img" src={homeBanner} alt="image"/>
					</div>
					<div className="inner-container top-btmPad">
						<div className="row-custom">
							<div className="col-left">
								<div className="search-aside">

									<div className="border-box">
										<h2 className="heading-bgColor">Notre Catalogue 2018</h2>
										<div className="border-boxInner">
											<div className="input-area">
												<input className="form-control" type="text" value={this.state.searchtxt}
													   placeholder="Entrez votre recherche" onBlur={this.getSearchtxt.bind(this)}
													   onChange={this.getSearchtxt}
													   />
											</div>
											<h4 className="bold-label">Prix</h4>
											<div className="range-row">
											<InputRange
													maxValue={this.state.maxAmount}
													minValue={this.state.minAmount}
													value={this.state.amountValue}
													onChange={this.changeAmountRange} />
											</div>
											<h4 className="bold-label">Type</h4>
											<div className="row">
												<div className="col-md-6 col-xs-12 check-row">
													<input type="checkbox" id="voile" onChange={this.getVoile}/>
													<label for="voile">Voile (3312)</label>
												</div>
												<div className="col-md-6 col-xs-12 check-row">
													<input type="checkbox" id="moteur" onChange={this.getMoteur}/>
													<label for="moteur">Moteur (6453)</label>
												</div>
											</div>
											<h4 className="bold-label">Etat</h4>
											<div className="row">
												<div className="col-md-6 col-xs-12 check-row">
													<input type="checkbox" onChange={this.getNuef}/>
													<label>NEUF (3312)</label>
												</div>
												<div className="col-md-6 col-xs-12 check-row">
													<input type="checkbox"  onChange={this.getOccasion}/>
													<label>OCCASION (6453)</label>
												</div>
											</div>
											<h4 className="bold-label">Années</h4>
											<div className="range-row">
											<InputRange
													maxValue={2018}
													minValue={1960}
													value={this.state.yearValue}
													onChange={this.changeYearRange} />
											</div>
											<h4 className="bold-label">Marques</h4>
											
											{this.getRenderedBrands().map(brand => (
											<div className="check-row">
												<input 
													name="brands[]" 
													id={'brand_' + brand.id} 
													type="checkbox" 
													value={brand.name}
													checked={this.state.brandFilters.includes(brand.name)}
													onChange={this.getBrandFilters}/>
												 <label htmlFor={"brand_"+brand.id}> {brand.name} ({brand.total_cars})</label>
											</div>
											))}
											
											<a href="javascript:void(0);" title="Plus de margues" className="text-link" onClick={this.showMoreBrandToggle}>
											{this.state.showMoreBrands ? 'less' : 'Plus de 	margues'}
											</a>
											<div className="btn-outer">
												<a href="javascript:void(0);" title="Rechercher"  onClick={this.getFilters.bind(this)}
												   className="btn-lightBlue">Rechercher</a>
											</div>
										</div>
									</div>

									<div className="space-box text-center">
										<img className="img-responsive" src={transparentLogo} alt="Logo"
											 width="145"
											 height="146"/>
									</div>

									<div className="border-box">
										<h2 className="heading-bgColor">RECEVER LA nEWSLETTER</h2>
										<div className="border-boxInner">
											<div className="image-bgBox">
												<p>DES DIZAINES DE NOUVELLES OFFRES CHAQUE JOUR</p>
												<i className="fa fa-star"></i>
												<p>UN CONTROLE DE QUALITE SUR CHAQUE ARTICLE</p>
												<i className="fa fa-star"></i>
												<p>UN SUIVI DETAILLé JUSQU’A Réception de votre bateau</p>
												<div className="input-bgBox">
													<input type="text" className="trans-control" placeholder="Email"/>
												</div>
											</div>
											<div className="btn-outer">
												<a href="javascript:void(0);" title="Rechercher"
												   className="btn-lightBlue btn-blue">Rechercher</a>
											</div>
										</div>
									</div>

								</div>
							</div>
							<div className="col-right">
								<h1 className="heading-lg">BIENVENUE SUR DICKINS <span>SPéCIALISTE EN IMPORTATION DE BATEAUX DES USA</span>
								</h1>
								<div className="round-ibox">
									<div className="colm-left">
										<span className="count-col">{totalCars}</span>
									</div>
									<div class="round-middle">
										<label>classer par</label>
										<div className="select-col">
											<select className="form-control">
												<option>Choisir un classement</option>
												<option>Classement two</option>
												<option>Classement three</option>
											</select>
										</div>
									</div>
									<div className="round-right">
										 <img className="img-responsive" src={importLogo} alt="" height="49" width="239"/>
									</div>
								</div>

								<div ref={this.listingRef} className="list-outer">
								{isLoaded ? (
									cars.map(car => (								
										<div>
											<div className="product-box">
												<div 
													style={{background: 'url(' + car.image + ') center', backgroundSize: 'cover'}} 
													className="list-image">
													<img className="img-responsive" src={car.image} alt="product"
														 width="243" height="135"/>
												</div>
												<div className="product-detail-box">
													<h3 className="product-detail-title">{car.title}</h3>
													<div className="product-detail-subtitle">{car.brand}, {car.model}</div>
													<div className="product-amount">{car.price} &euro;</div>
													<button type="button" className="btn blue-btn">Détails</button>
												</div>
											</div>
										</div>						  
									))
								) : (
									 <div class="loading">
										<div class="loader"></div>
									</div>
								)}
								
								</div>

								<div className="product-pagination text-center">
									<ul className="pagination">
										<li className="page-item">
											<a className="page-link" href="#" title="Previous" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
												<span className="sr-only">Previous</span>
											</a>
										</li>
										{isLoaded ? renderPageNumbers: null}
										<li className="page-item">
											<a className="page-link" href="#" title="Next" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
												<span className="sr-only">Next</span>
											</a>
										</li>
									</ul>
								</div>

							</div>
						</div>
					</div>
					<BusinessOfMonth/>
					<div className="bottom-bg-banner">
						<div className="inner-container">
							<div className="american-import text-center">
								<div className="white-logo">
									<img className="img-responsive" src={americanBoatLogo} alt="american-boat Import"
										 width="366"
										 height="99"/>
								</div>
								<div className="banner-caps-title">
									DIRECTEMENT LIVRé dAns LA marina de votre choix
								</div>
								<a href="javascript:void(0);" title="plus de témoignages" className="btn btn-black">NOS
									GARANTIES</a>
							</div>
						</div>
					</div>
					<Contact/>

				</div>

			);
		}
    }
}

export default Index;