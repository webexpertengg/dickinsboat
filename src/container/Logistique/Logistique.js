import React from 'react';
import productOne from '../../style/images/product1.jpg';
import productTow from '../../style/images/product2.jpg';
import productThree from '../../style/images/product3.jpg';
import productFour from '../../style/images/product4.jpg';
import productFive from '../../style/images/product5.jpg';
import productSix from '../../style/images/product6.jpg';
import logistiqueBg from '../../style/images/logistique.jpg';
import importLogo from "../../style/images/import-logo.png";
import logistiquePro1 from "../../style/images/logistique-pro1.jpg";
import logistiquePro2 from "../../style/images/logistique-pro2.jpg";
import logistique2 from "../../style/images/logistique2.jpg";
import evenements from '../../style/images/evenements.jpg';

import SearchTop from '../../component/SearchTop/TopSearch';
import BusinessOfMonth from '../../component/BusinessDetails/BusinessOfMonth';
import Contact from '../../component/ContactBottom/ContactBottom';


export default class Logistique extends React.Component {
    render() {
        return (
        <div>
            <div className="banner-section">
                <img className="img-responsive" src={logistiqueBg} alt="Dickins banner image" height="542" width="100%"/>
            </div>
            <div className="inner-pages">
                
                <div className="inner-container top-btmPad">
                    <SearchTop />
                </div>  
                <section className="achat-sec achat-sec1">
                    <div className="inner-container">
                        <div className="achat-head">
                            <h1 className="heading-lg">LOGISTIQUE </h1>
                            <div className="hr"></div>
                            <p>Chez Dickins nous sommes spécialistes en logistique d'importation de véhicules de
                                collection en provenance du conVtinent américain.</p>
                        </div>

                        <div className="achat-part">
                            <div className="row">
                                <div className="col-md-6">
                                    <img className="img-responsive" src={logistiquePro1} alt=""/>
                                </div>
                                <div className="col-md-6">
                                    <div className="achat-part-content">
                                        <h3 className="heading-black">PASSION ET PROFESSIONALISME</h3>
                                        <p>Passionnés et agissant pour le compte d'amateurs et collectionneurs, l'équipe
                                            de Fast & Retro s'implique intégralement dans les projets de nos potentiels
                                            clients.</p>
                                        <p>Chaque jour, nous facilitons de nombreuses acquisitions automobiles,
                                            réalisées outre-atlantique, et veillons à garantir le parfait déroulement de
                                            chacune d'entre elles. Depuis des années, notre activité s'est révélée étre
                                            une alternative, cohérente et toujours plus plébiscitée, permettant
                                            d'accéder à un exceptionnel inventaire en matiére de voitures de collection,
                                            en se prémunissant des moindres risques pouvant résulter d'une acquisition à
                                            distance.</p>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="achat-part">
                            <div className="row">

                                <div className="col-md-6">
                                    <div className="achat-part-content">
                                        <h3 className="heading-black les-head">LES éTAPES MAJEURES</h3>

                                        <div className="serial-lines">
                                            <div className="serial-no serial-no1">
                                                <span>1</span>
                                            </div>
                                            <div className="serial-no-content">
                                                <h4>DÉPÔT DE RÉSERVE</h4>
                                                <p>Le versement d'un acompte de 2000 euros est nécessaire pour initier
                                                    le processus de réservation et d'inspection de votre véhicule.</p>
                                            </div>
                                        </div>

                                        <div className="serial-lines">
                                            <div className="serial-no serial-no2">
                                                <span>2</span>
                                            </div>
                                            <div className="serial-no-content">
                                                <h4>RÉSERVATION DU BATEAU ET INSPECTION</h4>
                                                <p>Une fois la réservation enregistrée, via le versement d'un acompte,
                                                    un expert automobile agréé par l'état fédéral américain se rend sur
                                                    place entre 5 et 7 jours. Il est dépéché afin de procéder aux
                                                    contrôles administratifs et mécaniques nécessaires avec essai
                                                    routier. Si le véhicule correspond à l'annonce qui en a été faite et
                                                    qu'aucun vice caché n'est constaté, et avec votre accord, nous
                                                    confirmons la vente avec le propriétaire du véhicule.</p>
                                            </div>
                                        </div>

                                        <div className="serial-lines">
                                            <div className="serial-no serial-no3">
                                                <span>3</span>
                                            </div>
                                            <div className="serial-no-content">
                                                <h4>RÉGLEMENT DU VENDEUR ET CONSTITUTION DE VOTRE DOSSIER APRÉS
                                                    INSPECTION VALIDEE</h4>
                                                <p>Un virement international (avec SWIFT et IBAN), diminué de l'acompte
                                                    précédemment versé, vous sera demandé et devra étre effectué sous 72
                                                    heures pour profiter du taux de change du moment. Dés lors notre
                                                    service logistique constituera le dossier administratif et douanier
                                                    en votre nom.</p>
                                            </div>
                                        </div>

                                        <div className="serial-lines">
                                            <div className="serial-no serial-no4">
                                                <span>4</span>
                                            </div>
                                            <div className="serial-no-content">
                                                <h4>PRISE EN CHARGE DU BATEAU AUPRÉS DU VENDEUR</h4>
                                                <p>Notre agent transitaire organisera l'enlévement de votre véhicule et
                                                    sa livraison au terminal portuaire afin de le charger dans un
                                                    container de transport sécurisé. Au méme moment, une assurance
                                                    maritime tous risques est contractée afin de garantir un transport
                                                    et une livraison sans encombre de votre véhicule.</p>
                                            </div>
                                        </div>

                                        <div className="serial-lines">
                                            <div className="serial-no serial-no5">
                                                <span>5</span>
                                            </div>
                                            <div className="serial-no-content">
                                                <h4>ARRIVÉE DE VOTRE BATEAU ET LIVRAISON AU PORT</h4>
                                                <p>Trois à cinq semaines aprés l'enlévement de votre véhicule, celui-ci
                                                    arrivera au port de votre convenance. Une fois les formalités
                                                    accomplies, votre automobile sera enlevée par transporteur pour étre
                                                    livrée sur le lieu de votre choix. </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <img className="img-responsive" src={logistiquePro2} alt=""/>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div className="banner-bottom">
                        <img className="img-responsive" src={logistique2} alt=""/>
                    </div>
                    <div className="banner-content">
                        <div className="inner-container">
                            <h2 className="heading-md heading-md-copy">Directement livré dans le port de votre
                                choix</h2>
                            <div className="hr"></div>
                            <p>L'achat d'un bateau est l'une des réalisations majeures de la vie. Cependant, le défi de
                                trouver le yacht parfait au bon prix n'est pas un voyage à entreprendre à la légère ou
                                sans une main expérimentée pour vous guider.parfaite pour vous garantir une expérience
                                inoubliable.</p>
                            <div className="hr hr1"></div>
                            <div className="logistique-content">
                                <div className="row">
                                    <div className="col-sm-6">
                                        <div className="questions-content">
                                            <h3 className="heading-black">DOUANE ET IMMATRUCULATION</h3>
                                            <div className="hr"></div>
                                            <p>Une location de yacht de luxe offre tout ce que vous pouvez imaginer et
                                                plus, de la relaxation absolue à l'aventure à couper le souffle et aux
                                                moments précieux de la famille, c'est l'évasion ultime. La location d'un
                                                yacht vous permet d'explorer une variété de destinations tout en vous
                                                offrant tout le confort et les commodités d'une villa de luxe. Les
                                                possibilités sont infinies, mais votre courtier spécialisé dans
                                                l'affrètement découvrira vos besoins et vous guidera tout au long du
                                                processus de sélection, vous aidant ainsi à faire le choix final et à
                                                négocier le meilleur taux pour vous.</p>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="questions-content questions-content1">
                                            <h3 className="heading-black">DES QUESTIONS ?</h3>
                                            <p>Une location de yacht de luxe offre tout ce que vous pouvez imaginer et
                                                plus, de la relaxation absolue à l'aventure à couper le souffle et aux
                                                moments précieux de la famille.
                                                Une location de yacht de luxe offre tout ce que vous pouvez imaginer et
                                                plus, de la relaxation absolue à l'aventure à couper le souffle et aux
                                                moments précieux de la famille.</p>
                                            <div className="plus-btn">
                                                <button type="button" className="btn blue-btn">EN SAVOIR PLUS</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <BusinessOfMonth/>
                <Contact/>

            </div>
        </div>);
    }
}