import React from 'react';
import '../Home/Home'
import achatBanner from '../../style/images/achat-banner.jpg';
import productOne from '../../style/images/product1.jpg';
import productTow from '../../style/images/product2.jpg';
import productThree from '../../style/images/product3.jpg';
import productFour from '../../style/images/product4.jpg';
import productFive from '../../style/images/product5.jpg';
import productSix from '../../style/images/product6.jpg';
import americanBoatLogo from '../../style/images/american-boat-logo.png';
import transparentLogo from '../../style/images/transparent-logo.png';
import importLogo from '../../style/images/import-logo.png';
import evenements from '../../style/images/evenements.jpg';
import achatPartOne from '../../style/images/achat-part-img1.jpg';
import achatPartTow from '../../style/images/achat-part-img2.jpg';

import SearchTop from '../../component/SearchTop/TopSearch';
import Contact from '../../component/ContactBottom/ContactBottom';


export default class Achat extends React.Component {
    render() {
        return (<div className="banner-section">
                <img className="img-responsive" src={achatBanner} alt="Dickins banner image" height="542" width="100%"/>
                <div className="inner-pages">
                    
                    <div className="inner-container top-btmPad">
                        <SearchTop />
                    </div> 
                    <section className="achat-sec">
                        <div className="inner-container">
                            <div className="achat-head">
                                <h1 className="heading-lg">ACHETER UN BATEAU d’occasion </h1>
                                <div className="hr"></div>
                                <p>Dickins propose la plus grande flotte de bateaux à vendre dans le monde qui comprend
                                    une
                                    large gamme de bateaux y compris les bateaux a moteur et bateaux à voile
                                    classiques. </p>
                            </div>
                        </div>

                        <div className="achat-part">
                            <div className="row">
                                <div className="col-md-6">
                                    <img className="img-responsive" src={achatPartOne} alt="product"/>
                                </div>
                                <div className="col-md-6">
                                    <div className="achat-part-content">
                                        <p>Acquérir un bateau est un investissement extrêmement privé, engendrant une
                                            implication à la fois émotionnelle et financière. La recherche du bateau
                                            idéal
                                            nécessite un encadrement d’experts dotés d’une véritable connaissance du
                                            marché
                                            et d’une vision personnelle.</p>
                                        <p>Comment un acheteur peut-il identifier une vraie affaire ? Le meilleur yacht,
                                            le
                                            meilleur prix ? C’est ici tout le rôle des conseillers et specialistes
                                            DICKINS :
                                            vous guider pas à pas dans le processus d’achat.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="achat-part">
                            <div className="row">

                                <div className="col-md-6">
                                    <div className="achat-part-content achat-part-content2">
                                        <div className="testimonial-icon"></div>
                                        <h3 className="heading-black">NOS CONSEILLERS A VOTRE SERVICE</h3>
                                        <p>Chaque conseiller DICKINS fait partie d’un réseau mondial de veille
                                            commerciale.
                                            Où que vous nous contactiez dans le monde, vous tirerez profit de nombreuses
                                            années d’expérience et d’une vision globale des meilleures opportunités.
                                            L’équipe de conseillers hautement qualifiés de DICKINS travaille pour le
                                            compte
                                            des acheteurs afin de sélectionner des yachts de tous les continents.</p>
                                        <p>Grâce à la collaboration étroite des membres de notre équipe et à notre
                                            implication dans un nombre important de transactions de bateau , nous avons
                                            une
                                            vision globale inégalée sur les opportunités en vigueur sur le marché.</p>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <img className="img-responsive" src={achatPartTow} alt="product"/>
                                </div>

                            </div>
                        </div>

                    </section>
                    <section className="products">
                        <div className="inner-container top-btmPad">

                            <div className="row">
                                <div className="col-md-6">
                                    <h2 className="heading-md">LES DERNIERS ARRIVAGES</h2>
                                </div>
                                <div className="col-md-6">
                                    <h4 className="range-head">Filters:</h4>
                                    <div className="range">
                                        <div className="range-slide1">
                                            <div className="range-row">
                                                <input className="range-slider3" type="hidden" value=""
                                                       style={{display: 'none'}}/>
                                            </div>
                                        </div>

                                        <div className="range-slide2">
                                            <div className="range-row">
                                                <input className="range-slider4" type="hidden" value="1960,2018"
                                                       style={{display: 'none'}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="check-slide">
                                        <div className="check-row">
                                            <input type="checkbox"/>
                                            <label>VOILE</label>
                                        </div>
                                        <div className="check-row">
                                            <input type="checkbox"/>
                                            <label>MOTEUR</label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="list-outer">
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productOne} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2010 CODSCASA MEGA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, SEA RAY</div>
                                            <div className="product-amount">350,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productTow} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2012 WANTA SSA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, VICKING</div>
                                            <div className="product-amount">450,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productThree} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2014 ODDISSEY</h3>
                                            <div className="product-detail-subtitle">Hord Bord, HATTERAS</div>
                                            <div className="product-amount">390,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productFour} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2010 CODSCASA MEGA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, SEA RAY</div>
                                            <div className="product-amount">350,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productFive} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2012 WANTA SSA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, VICKING</div>
                                            <div className="product-amount">450,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productSix} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2014 ODDISSEY</h3>
                                            <div className="product-detail-subtitle">Hord Bord, HATTERAS</div>
                                            <div className="product-amount">390,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productOne} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2010 CODSCASA MEGA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, SEA RAY</div>
                                            <div className="product-amount">350,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productTow} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2012 WANTA SSA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, VICKING</div>
                                            <div className="product-amount">450,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productThree} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2014 ODDISSEY</h3>
                                            <div className="product-detail-subtitle">Hord Bord, HATTERAS</div>
                                            <div className="product-amount">390,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productFour} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2010 CODSCASA MEGA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, SEA RAY</div>
                                            <div className="product-amount">350,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productFive} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2012 WANTA SSA</h3>
                                            <div className="product-detail-subtitle">Hord Bord, VICKING</div>
                                            <div className="product-amount">450,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="product-box">
                                        <div className="list-image">
                                            <img className="img-responsive" src={productSix} alt="product"
                                                 width="243" height="135"/>
                                        </div>
                                        <div className="product-detail-box">
                                            <h3 className="product-detail-title">2014 ODDISSEY</h3>
                                            <div className="product-detail-subtitle">Hord Bord, HATTERAS</div>
                                            <div className="product-amount">390,000 €</div>
                                            <button type="button" className="btn blue-btn">Plus de Détails</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="product-pagination text-center">
                                <ul className="pagination">
                                    <li className="page-item">
                                        <a className="page-link" href="#" title="Previous" aria-label="Previous">
                                            <span aria-hidden="true">«</span>
                                            <span className="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <li className="page-item active"><a className="page-link" href="#">1</a></li>
                                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                                    <li className="page-item"><a className="page-link" href="#">4</a></li>
                                    <li className="page-item"><a className="page-link" href="#">5</a></li>
                                    <li className="page-item"><a className="page-link" href="#">6</a></li>
                                    <li className="page-item"><a className="page-link" href="#">7</a></li>
                                    <li className="page-item">
                                        <a className="page-link" href="#" title="Next" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                            <span className="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </section>

                    <Contact/>
                </div>
            </div>
        )
    }
}