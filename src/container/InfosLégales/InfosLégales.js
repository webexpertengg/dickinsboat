import React from 'react';
import temoignages from '../../style/images/temoignages.jpg';

import SearchTop from '../../component/SearchTop/TopSearch';


export default class Logistique extends React.Component {
    render() {
        return (
        <div>
            <div className="banner-section">
                <img className="img-responsive" src={temoignages} alt="Dickins banner image" height="542" width="100%"/>
            </div>
            <div className="inner-pages temoignages">
                
                <div className="inner-container top-btmPad">
                    <SearchTop />
                </div>  
                <div className="inner-container">
                    <div className="contact-heading">
                        <h1 className="heading-lg">INFOS LEGALES</h1>
                        <div className="hr"></div>
                    </div>
                    <div className="infos-content">
                        <h3 className="heading-black">Conditions Gènèrales de Vente</h3>
                        <h4>ARTICLE 1</h4>
                        <h4>OBJET</h4>
                        <p>Le présent contrat est conclu entre La société Dickins LLC prestataire logistique et transports et le CLIENT, via le site internet www.Dickins.com </p>
                        <p>Sur la base du contrat de prestations de service , conclu à distance sur un mode électronique, la société Dickins LLC s’identifie auprès du consommateur. </p>
                        <p>Dickins LLC est reconnue pour la qualité de son service import export de véhicules américains à destination du monde entier. </p>
                        <p>Son expérience lui a permis de développer des relations privilégiées avec des partenaires fiables ayant des compétences complémentaires qui lui permettent de garantir la qualité de l’ensemble des prestations en matière de logistique internationale ,d’importation et d’exportation, de transport routier à travers les États unis, de transport maritime, d’accompagnement douanier et de gestion de documents officiels.</p>
                        <p>Le societe Dickins LLC se met en contact avec les vendeurs américains a la demande expresse des acheteurs : les prix de vente formulés en Euros sont réglés soit directement au vendeur par les acheteurs ou a Dickins CORPORATION qui intègre ses frais de logistique et transports au montant de la transaction pour faciliter, simplifier et sécuriser l’opération globale. </p>
                        <p>Dickins LLC (société américaine enregistrée au bureau du secrétaire de l’état du Nevada dont le siège social est situé 848 N Rainbow Blvd, Las Vegas ,NV,89107 n’intervient en aucune manière dans l’établissement des prix de vente. Dickins LLC se charge uniquement de l’acheminement et de la circulation des marchandises via l’intervention de ses agents et prestataires appliquant les taxations en vigueur dans les pays de départ comme dans ceux de destination. </p>
                        <p>Spècificites des prestations et services </p>
                        <p>Dickins CORPORATION transmet au consommateur les caractéristiques principales des véhicules commandés, dont elle assure la finalisation de l’achat aux États Unis auprès de vendeurs américains et l’acheminement international en qualité de mandataire. </p>
                        <p>Ces caractéristiques qualitatives et quantitatives figurent sur les différents sites professionnels et publics dont l’accès est gratuit. </p>
                        <p>Le client reconnait qu’il a porté une attention toute particulière à la lecture de l’annonce, pour laquelle il a pu utiliser le traducteur (Service de traduction fournit à titre gratuit par la société Google basée 1600 Amphitheatre Parkway, Mountain View, CA 94043, États-Unis). </p>
                        <p>Le bien est livré par bateau à la zone portuaire élue par le client. Il est possible de solliciter la livraison du véhicule à domicile, dans ce cas, le client devra se rapprocher de Dickins LLC qui le mettra en rapport avec son transporteur afin de lui faire bénéficier des meilleures conditions tarifaires.</p>
                        <p>A l’arrivée du véhicule au port de destination, le client devra acquitter les frais de douane, de fret ainsi que la TVA auprès du transitaire désigné. </p>
                        <p>Les photographies illustrant les produits ou services proposés sur les sites Web n’ont qu’une valeur indicative et non contractuelle. En aucun cas, la responsabilité de Dickins LLC ne pourra être engagée en cas d’éventuelles erreurs ou de non conformité entre l’annonce passée et l’article livré. </p>
                        <p>Dickins CORPORATION n’étant pas une société de vente, mais une société de logistique d’importation internationale, ne peut procéder à aucun échange et ne peut pas reprendre les véhicules. </p>
                        <p>Le client reconnaît qu’il a été informé qu’il avait la possibilité de consulter une équipe de conseillers mise à sa disposition par Dickins LLC. </p>
                        <p>Le client reconnaît qu’il a été informé de la possibilité de contacter un spécialiste recommandé par Dickins LLC afin d’être informé sur les homologations des véhicules et des conditions de circulation sur le réseau routier public ainsi que du montant des taxes à acquitter, consécutives à l’importation du véhicule. </p>
                        <p>La gamme de services proposée par Dickins LLC, a pour objectif d’assurer un maximum de facilités et de confort à ses clients. Ces professionnels, non rattachés à notre société, qu’ils soient transporteurs, compagnies d’assurances, garagistes, experts ou encore services d’homologation, sont seuls responsables de la qualité de leur prestation. Nous rappelons aux utilisateurs de Dickins LLC, que notre unique prestation relève d’un service de logistique d’importation et, qu’au travers de notre site internet, ne faisons que mettre en relation des vendeurs et des acheteurs. </p>
                        <p>Les conseillers a toute demande spécifique aboutit à la mise en relation avec un de nos conseillers, en charge d’apporter des informations complémentaires concernant l’utilisation du site www.Dickins.com, ainsi que le mode opératoire relatif à l’importation des articles dont les internautes souhaitent faire l’acquisition. </p>
                        <p>Ces conseillers sont autorisés à donner leur opinion concernant les offres, par leur connaissance des remontées d’informations du marché sur lequel ils officient. Le client, et lui seul, est à même d’en mesurer la recevabilité, de même qu’il est seul responsable de sa décision d’achat. </p>
                        <h4>ARTICLE 2 </h4>
                        <h4>ENGAGEMENT</h4>
                        <p>L’acceptation de la commande et du présent contrat se réalise grâce à la preuve de virement et/ou par le virement. Lorsque l’acheteur envoi le virement il est irrévocablement lié. </p>
                        <p>Son acceptation ne pourra être ultérieurement remise en cause. En effet, les systèmes d’enregistrement automatiques mis en place par Dickins CORPORATION sont considérés comme valant preuve de la conclusion du présent contrat. </p>
                        <h4>ARTICLE 3 </h4>
                        <h4>PRIX </h4>
                        <p>L’ensemble des prix sont indiqués TTC (taxe a 5,5%) et en Euros. </p>
                        <h4>ARTICLE 4 </h4>
                        <h4>CONFIRMATION DE LA COMMANDE </h4>
                        <p>Le consommateur recevra une confirmation de la commande sous 72 heures au plus tard, délai nécessaire aux contrôleurs américains mandatés par Dickins LLC pour vérifier l’identité du vendeur et la conformité du véhicule à l’annonce. </p>
                        <p>Cette confirmation aura lieu par voie de courrier électronique (e-mail).</p>
                        <p>Une copie des documents officiels sera transmise à Dickins LLC afin qu’elle puisse entamer le processus d’importation. </p>
                        <p>Le contenu de ces confirmations de commande est archivé par Dickins LLC. Elles sont considérées comme preuve du consentement du consommateur à la présente transaction et a sa date. </p>
                        <h4>ARTICLE 5 </h4>
                        <h4>PROCEDURES DE PAIEMENT </h4>
                        <p>Le paiement de cette somme permet de bloquer immédiatement la vente au profit du client. </p>
                        <p>Le client reconnaît avoir été informé que le paiement de cette somme est aussi destiné à garantir une prise en charge complète et personnalisée par l’équipe de Dickins LLC, afin de lui permettre de finaliser son acquisition. </p>
                        <p>-Paiement aux États Unis: lors de la confirmation de la commande d’une facture correspondant aux frais de logistique d’importation émise par Dickins LLC dont le montant a été communiqué au client par le conseiller lors de l’établissement de l’ordre de mission. </p>
                        <p>Cette facture devra être réglée dans un délai de trois jours à compter de sa réception par virement bancaire, ce afin d’assurer le respect des plannings de réservation des prestataires de Dickins LLC Une fois ce paiement effectué l’achat sera définitivement validé et le vendeur fera parvenir à Dickins LLC les documents de vente qui seront envoyés au client par Dickins CORPORATION – Paiement dans un délai de trois jours à compter de la validation de l’achat de prix du véhicule par virement en dollars US. </p>
                        <p>Ce procédé permet au client d’avoir toutes les garanties nécessaires et de bénéficier du taux de change le plus avantageux. </p>
                        <p>Le client devra justifier par mail à Dickins LLC du fait que les virements ont été effectués dés leur exécution, ce afin de ne pas ralentir le processus d’importation. </p>
                        <p>Toutes demandes de factures aditionnelles, n’incluant pas la facture initiale, sera facturee 50 euros. </p>
                        <p>Si toutefois le client ne paye pas la dite facture dans le délai respecté ( 3 jours ) et que le véhicule n’est plus disponible ultérieurement , le montant de la somme versé sera remis en jeu pour l’achat d’un prochain véhicule. Aucun remboursement ne sera effectué. </p>
                        <h4>ARTICLE 6 </h4>
                        <h4>RESERVE D ARGENT </h4>
                        <p>Il est possible au client de se constituer une réserve d’argent , disponible à tout moment afin d’accélérer les transactions et de transmettre un ordre d’acquisition en temps réel à Dickins LLC dans le cadre d’une vente aux enchères par exemple. </p>
                        <p>Le client peut mettre a disposition de Dickins LLC une somme definie en dépôt pour lui permettre d’enchérir immédiatement pour son compte .</p>
                        <p>Si le véhicule ou le produit est adjugé au client au prix qu’il souhaitait Dickins LLC devra réserver le véhicule ou le produit, pour bloquer la vente en faveur du client représenté. Dans l’hypothèse ou le budget fixé par le client ne lui permet pas de remporter l’enchère, celui-ci recevra un avoir immédiatement. </p>
                        <p>LDans l’hypothèse ou le client ne peut pas s’acquitter ultérieurement du prix fixe aux enchères, le dépôt sera définitivement perdu car conserve par le service des enchères apres un mois. </p>
                        <h4>ARTICLE 7 </h4>
                        <h4>LIVRAISON </h4>
                        <p>Dickins LLC s’engage à livrer le bien vendu dans un délai, de soixante (60) jours à compter du paiement. Ce délai pourra être prolongé en raison d’événements exceptionnels tels que grève des transports, intempéries, incendie etc…, ou tout simplement sur demande du client pour effectuer des reparations, remise en etat et/ou retard des transports domestiques. </p>
                        <p>Le délai de livraison pourra être de 5 à 8 semaines en fonction de la localisation du véhicule sur le territoire américain.</p>
                        <p>En aucun cas Dickins LLC ne pourra être tenue pour responsable d’un retard de livraison occasionné par un événement indépendant de sa volonté. </p>
                        <p>Si pour quelque raison que ce soit le véhicule devait se trouver indisponible, le consommateur en sera informé dans les plus brefs délais et pourra alors l’option de relancer sa recherche. </p>
                        <p>En cas d’indisponibilité la demande de restitution du prix devra être effectuée auprès du vendeur qui en a encaissé le montant. </p>
                        <p>La livraison aura lieu au port choisi par Dickins LLC qui reconnait avoir été informé que les frais de livraison sont à sa charge. </p>
                        <p>Les biens commandés voyagent avec assurance. Toute contestation doit être formulée par lettre recommandée avec A.R auprès du transporteur dans un délai de trois (3) jours à compter de la livraison. </p>
                        <h4>ARTICLE 8 </h4>
                        <h4>GARANTIE ET SERVICE APRES-VENTE </h4>
                        <p>En aucun cas la responsabilité de Dickins LLC ne pourra être recherchée et ce pour quelque cause que ce soit. </p>
                        <h4>ARTICLE 9 </h4>
                        <h4>SECURITE DES TRANSACTIONS </h4>
                        <p>La société déclare avoir mis en œuvre les dispositifs de sécurité requis pour sécuriser les transactions, savoir : </p>
                        <p>-Application acompte paiement sécurisé : Paypal.com </p>
                        <p>-Transaction par carte bleu au Téléphone </p>
                        <p>-Virement bancaire </p>
                        <h4>ARTICLE 10 </h4>
                        <h4>ANNULATION DE VOTRE ACHAT </h4>
                        <p>En cas d'annulation de votre achat, les frais d'agence, de transport et de réservation du conteneur seront perdus. La différence des fonds sera renvoyée au client lorsque le véhicule aura été revendu. </p>
                        <h4>ARTICLE 11 </h4>
                        <h4>REGLEMENT DES LITIGES </h4>
                        <p>En cas de contestation relative à l’interprétation et à l’exécution du présent contrat, les parties conviennent que, préalablement à toute action judiciaire, elles s’engagent à soumettre leurs différends à la médiation du Centre de médiation et d’arbitrage dans l’Etat de Nevada a Las Vegas. En cas d’échec de la médiation, tout litige né du présent contrat relèvera de la compétence des autorités américaines dans l’Etat du Nevada a Las Vegas. </p>
                        <h4>ARTICLE 12 </h4>
                        <h4>LOI APPLICABLE</h4>
                        <p>Les présentes Conditions sont régies par la Législation américaine </p>
                        <p>Mentions lègales </p>
                        <p>Editeur du Site</p>
                        <p>Le site Dickins LLC est édité par Dickins llc. </p>
                        <p>Droits d'auteur</p>
                        <p>Le site DickinsCorporation est protégé par les dispositions du Code de la propriété intellectuelle, notamment par celles relatives à la propriété littéraire et artistique et au droit des marques. </p>
                        <p>CNIL – Protection des données personnelles Les informations que nous sommes amenés à recueillir proviennent : – soit de l’inscription volontaire d’une adresse e-mail de votre part vous permettant de recevoir notre newsletter, – soit d’un abonnement de votre part aux services proposés par notre site, – soit de la saisie complète de vos coordonnées par vos soins à l’occasion d’une opération événementielle. Ces informations nous permettent de mieux vous connaître. Elles pourront être utilisées, en outre, pour vous informer de l’existence de nos produits et services. Vous disposez d’un droit d’accès,de modification, de rectification et de suppression des données qui vous concernent (art. 34 de la loi « Informatique et Libertés » n° 78-17 du 6 janvier 1978 ). </p>
                        <p>Cookies Dickins LLC vous informe qu’un cookie est placé dans votre ordinateur lorsque vous naviguez sur son site. Un cookie ne nous permet pas de vous identifier. De manière générale, il enregistre des informations relatives à la navigation de votre ordinateur sur notre site (les pages que vous avez consultées, la date et l’heure de la consultation, etc.) que nous pourrons lire lors de vos visites ultérieures. Son but unique est de mettre en place un comptage du nombre de visiteurs et de limiter éventuellement le nombre de délivrance d’une même bannière publicitaire à un même utilisateur. La durée de conservation de ces informations dans votre ordinateur est de un an. Nous vous informons que vous pouvez vous opposer à l’enregistrement de « cookies » en désactivant cette option dans les paramètres de votre navigateur.</p>
                    </div>
                </div>

            </div>
        </div>);
    }
}