import React from 'react';
import achatBanner from '../../style/images/achat-banner.jpg';

import SearchTop from '../../component/SearchTop/TopSearch';
import Contact from '../../component/ContactBottom/ContactBottom';


export default class Logistique extends React.Component {
    render() {
        return (
        <div>
            <div className="banner-section">
                <img className="img-responsive" src={achatBanner} alt="Dickins banner image" height="542" width="100%"/>
            </div>
            <div className="inner-pages temoignages">
                <div className="inner-container top-btmPad">
                    <SearchTop />
                </div>
                <section className="achat-sec">
                    <div className="inner-container">
                        <div className="achat-head">
                            <h1 className="heading-lg">PLAN-DU-SITE</h1>
                            <div className="hr"></div>
                            <div className="plan-links">
                                <ul>
                                    <li><a href="/" title="Accueil">Accueil</a></li>
                                    <li><a href="/achat-occasion" title="Achat occasion">Achat occasion</a></li>
                                    <li><a href="/logistique" title="Logistique">Logistique</a></li>
                                    <li><a href="/dickins-and-co" title="Dickins & Co">Dickins & Co</a></li>
                                    <li><a href="/temoignage" title="Temoignages">Temoignages</a></li>
                                    <li><a href="/infos-légales" title="Infos legales">Infos legales</a></li>
                                </ul>
                            </div>
                         
                        </div>
                    </div>
                </section>
                 <Contact />
            </div>
        </div>);
    }
}