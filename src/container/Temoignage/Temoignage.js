import React from 'react';
import temoBanner from '../../style/images/temoignages.jpg';

import SearchTop from '../../component/SearchTop/TopSearch';
import BusinessOfMonth from '../../component/BusinessDetails/BusinessOfMonth';
import Contact from '../../component/ContactBottom/ContactBottom';


export default class termon extends React.Component {
    render() {
        return (
        <div>
            <div className="banner-section">
                <img className="img-responsive" src={temoBanner} alt="Dickins banner image" height="542" width="100%"/>
            </div>
            <div className="inner-pages temoignages">

                <div className="inner-container top-btmPad">
                    <SearchTop />
                </div>
                <div className="inner-container">
                    <div className="achat-head">
                        <h1 className="heading-lg">TEMOIGNAGES</h1>
                        <div className="hr">
                        </div>
                        <p>Votre avis nous interesse! Notre volonté est de toujours apprendre de nos clients pour vous accompagner au mieux. Donnez votre avis en remplissant le formulaire placé à cet effet.</p>
                    </div>
                    <div className="testimonial-contents">
                        <div className="testimonial-icon"></div>
                        <div className="row">
                            <div className="col-md-7">
                                <div className="test-text">
                                    <h3>Francis S.</h3>
                                    <p>Merci Alex pour votre professionalisme après l'achat de mon voilier chez vous. 
                                       Pour ma part je recommanderais votre societe a ma famille et a mes amis
                                       Merci également pour votre gentillesse et disponibilité.<br></br>
                                       Je recommande DICKINS sans hésitation pour les prix très compétitifs, commande, délai et livraison sans surprise. </p>
                                       <p>Pour ma part je recommanderais votre societe a ma famille et a mes amis Encore merci pour tout.</p>
                                    <div className="hr"></div>
                                    <h3>Diane A.</h3>
                                    <p>Merci Alex pour votre professionalisme après l'achat de mon voilier chez vous. Merci également pour votre gentillesse et disponibilité. Je recommande DICKINS sans hésitation pour les prix très compétitifs, commande, délai et livraison sans surprise. Pour ma part je recommanderais votre societe a ma famille et a mes amis Encore merci pour tout.</p>
                                    <div className="hr"></div>
                                    <h3>Charles C.</h3>
                                    <p>Merci Alex pour votre professionalisme après l'achat de mon voilier chez vous. Merci également pour votre gentillesse et disponibilité. Je recommande DICKINS sans hésitation pour les prix très compétitifs, commande, délai et livraison sans surprise. Pour ma part je recommanderais votre societe a ma famille et a mes amis Encore merci pour tout.</p>
                                    <a href="">Afficher plus de témoignages</a>
                                </div>

                            </div>
                            <div className="col-md-5">
                                <div className="form">
                                    <div className="textarea">
                                        <label>Ecrivez votre témoignage:</label>
                                        <textarea rows="10" cols="50" name="comment" form="usrform"></textarea>
                                    </div>
                                    <div className="username">
                                        <label>Name: </label>
                                        <input className="user-input" type="text" name="usrname" />
                                        <a className="envoyer-btn" href="">ENVOYER</a>
                                    </div>
                                    <div className="share">
                                        <label>Share:</label>
                                        <input type="checkbox" name="" value="" />
                                        <a href="javascript:void(0);" title="Facebook"><i class="fa fa-facebook-f"></i></a>
                                        <input type="checkbox" name="" value="" />
                                        <a href="javascript:void(0);" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <BusinessOfMonth/>
                <Contact/>
            </div>
        </div>);
    }
}