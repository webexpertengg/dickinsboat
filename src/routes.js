import React from 'react';
import {Route, Switch} from 'react-router';
import Home from './container/Home';
import Achat from './container/Achat';
import Logistique from './container/Logistique';
import Temoignage from './container/Temoignage';
import DickinsCo from './container/DickinsCo';
import Contact from './container/Contact';
import InfosLégales from './container/InfosLégales';
import PlanDuSite from './container/PlanDuSite';

const routes = (
  <Switch>
    <Route exact path="/" component={Home}/>
    <Route exact path="/achat-occasion" component={Achat}/>
    <Route exact path="/logistique" component={Logistique}/>
    <Route exact path="/temoignage" component={Temoignage}/>
    <Route exact path="/dickins-and-co" component={DickinsCo}/>
    <Route exact path="/contact" component={Contact}/>
    <Route exact path="/infos-légales" component={InfosLégales}/>
    <Route exact path="/plan-du-site" component={PlanDuSite}/>
  </Switch>
);

export default routes;